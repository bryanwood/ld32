var game = new Phaser.Game(800,200, Phaser.CANVAS, 'game', {preload:preload, create:create, update:update, render:render});

function preload(){
  game.load.image('drawers', 'assets/drawers.png');
  game.load.image('floor', 'assets/floor.png');
  game.load.image('knob-1', 'assets/knob-1.png');
  game.load.image('knob-2', 'assets/knob-2.png');
  game.load.image('drawer-top', 'assets/drawer-top.png');
  game.load.image('drawer-foot', 'assets/drawer-foot.png');
  game.load.image('sock', 'assets/sock.png');
  game.load.spritesheet('drawer-draw', 'assets/drawer-draw.png', 44, 28, 2);
  game.load.image('wallpaper', 'assets/wallpaper.png');
  game.load.image('painting','assets/painting.png');
  game.load.image('gameover','assets/gameover.png');
}


var cursors;
var keyA, keyQ, keyZ;

function createBulletGroup(max, kind){
  var group = game.add.group();
  group.enableBody = true;
  group.createMultiple(max, kind);
  group.setAll('anchor.x', 0.5);
  group.setAll('anchor.y', 0.5);
  return group;
}

var PLAYER_X = 24;
var PLAYER_Y = 100;

var isStarted = false;

function create(){

  game.stage.backgroundColor = 0xffffff;
  game.add.tileSprite(0,0,800,600, 'wallpaper');
  game.add.tileSprite(0,168,800,32, 'floor');
  //physics
  game.physics.startSystem(Phaser.Physics.ARCADE);
  game.add.sprite(700, 20, 'painting');

  drawer.bullets = createBulletGroup(1, 'knob-1');

  game.add.sprite(PLAYER_X,PLAYER_Y, 'drawer-top');
  game.add.sprite(PLAYER_X,PLAYER_Y+6+28+28+28, 'drawer-foot');
  drawer.Q.player = game.add.sprite(PLAYER_X,PLAYER_Y+6,'drawer-draw');

  drawer.Q.player.animations.add('open',[2], 1, false, true);
  drawer.Q.player.animations.add('close',[1], 1, false, true);
  drawer.Q.player.animations.play('close');
  game.physics.arcade.enableBody(drawer.Q.player);

  drawer.A.player = game.add.sprite(PLAYER_X,PLAYER_Y+34,'drawer-draw');
  drawer.A.player.animations.add('open',[2], 1, false, true);
  drawer.A.player.animations.add('close',[1], 1, false, true);
  drawer.A.player.animations.play('close');
  game.physics.arcade.enableBody(drawer.A.player);

  drawer.Z.player = game.add.sprite(PLAYER_X,PLAYER_Y+62,'drawer-draw');
  drawer.Z.player.animations.add('open',[2], 1, false, true);
  drawer.Z.player.animations.add('close',[1], 1, false, true);
  drawer.Z.player.animations.play('close');
  game.physics.arcade.enableBody(drawer.Z.player);

  socks = game.add.group();
  socks.enableBody = true;
  socks.createMultiple(10, 'sock');
  socks.setAll('anchor.x', 0.5);
  socks.setAll('anchor.y', 0.5);

  keyQ = game.input.keyboard.addKey(Phaser.Keyboard.Q);
  keyA = game.input.keyboard.addKey(Phaser.Keyboard.A);
  keyZ = game.input.keyboard.addKey(Phaser.Keyboard.Z);

  var style = {font:"14px Arial", fill:"#000000", align:"center"};
  game.add.text(PLAYER_X+9,PLAYER_Y+12, "Q", style);
  game.add.text(PLAYER_X+9,PLAYER_Y+40, "A", style);
  game.add.text(PLAYER_X+9,PLAYER_Y+68, "Z", style);
  scoreText = game.add.text(710,30, "0", style);
}
var scoreText;
var score = 0;
var socks;
var sock;
var nextSock = 0;
var isGameover = false;

var hasBullet = false;

var drawer = {
  bullets:undefined,
  bullet:undefined,
  bulletTime:0,
  Q: {
    player:undefined,
    startX:PLAYER_X+40,
    startY:PLAYER_Y+22,
    velocity:250,
    cooldown:500
  },
  A: {
    player:undefined,
    startX:PLAYER_X+40,
    startY:PLAYER_Y+48,
    velocity:250,
    cooldown:500
  },
  Z: {
    player:undefined,
    startX:PLAYER_X+40,
    startY:PLAYER_Y+76,
    velocity:250,
    cooldown:500
  }
};
var bullets = {};
var bullet = {};
var bulletTime = {Q:0,A:0,Z:0};

function resetAnimations(group){
  if(game.time.now > drawer.bulletTime-30){
    group.player.animations.play('close');
  }
}

function getSpeed(score){
  if(score<10) return 50;
  if(score<25) return 75;
  if(score<50) return 100;
  if(score<100) return 150;
  if(score<125) return 200;
  if(score<150) return 250;
  if(score<175) return 300;
  if(score<200) return 350;
  return 500;
}

function fireBullet(group){
  if(game.time.now > drawer.bulletTime && !hasBullet && !isGameover && isStarted){

    drawer.bullet = drawer.bullets.getFirstExists(false);
    if(drawer.bullet){
      group.player.animations.play('open');
      drawer.bullet.reset(group.startX, group.startY);
      drawer.bullet.lifespan = 4000;
      drawer.bullet.body.velocity.x = group.velocity;
      drawer.bulletTime = game.time.now+group.cooldown;
    }
  }
}

function update(){

  if(game.time.now > nextSock && isStarted){
    sock = socks.getFirstExists(false);
    if(sock){
      sock.reset(750, PLAYER_Y+Math.floor(Math.random()*3)*24+24);
      sock.lifespan = 20000;
      sock.body.velocity.x = -Math.floor(Math.random()*50+getSpeed(score));
      sock.body.acceleration.x = -getSpeed(score)*0.5;
      sock.body.angularVelocity = -110;
      nextSock = game.time.now + Math.floor(Math.random()*2000 + 250);
    }
  }

  if(keyQ.isDown){
    fireBullet(drawer.Q);
  }
  if(keyA.isDown){
    fireBullet(drawer.A);
  }
  if(keyZ.isDown){
    fireBullet(drawer.Z);
  }

  resetAnimations(drawer.Q);
  resetAnimations(drawer.A);
  resetAnimations(drawer.Z);

  game.physics.arcade.overlap(socks, drawer.Q.player, function(player,sock){
    sock.kill();
    gameover();
  });
  game.physics.arcade.overlap(socks, drawer.A.player, function(player,sock){
    sock.kill();
    gameover();
  });
  game.physics.arcade.overlap(socks, drawer.Z.player, function(player,sock){
    sock.kill();
    gameover();
  });

  game.physics.arcade.overlap(drawer.bullets, socks, function(sock, bullet){
    sock.kill();
    bullet.kill();
    hasBullet = false;
    score = score+1;
  });
  scoreText.setText(""+score);
}

function gameover(){
  isGameover = true;
  game.add.sprite(320,100,'gameover');
}

function render(){
}

function start(){
  isStarted = true;
  document.getElementById("start").style.visibility = 'hidden';
}
